resource "aws_instance" "webservers" {
	count = length(var.subnets_cidr) 
	ami = var.webservers_ami
	instance_type = var.instance_type
	security_groups = [aws_security_group.webservers.id]
	subnet_id = element(aws_subnet.public.*.id,count.index)
	user_data = file("install_httpd.sh")

	tags = {
	  Name = "BrettServer${count.index}"
	}
}

resource "aws_launch_configuration" "web" {
  name_prefix = "web-"

  image_id = var.webservers_ami 
  instance_type = var.instance_type
  
  security_groups = [ aws_security_group.webservers.id ]
  associate_public_ip_address = true

  user_data = file("install_httpd.sh")

 # tags = {
 #   Name = "BrettServer${count.index}"
 # }

  lifecycle {
    create_before_destroy = true
  }
}

#resource "aws_autoscaling_group" "bar" {
#  availability_zones = var.azs
#  desired_capacity   = 5
#  max_size           = 8
#  min_size           = 2

#  launch_template {
#    id      = aws_launch_template.brett_launch_template.id
#    version = "$Latest"
#  }
#}
