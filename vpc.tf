# VPC
resource "aws_vpc" "brett_vpc" {
  cidr_block       = var.vpc_cidr
  tags = {
    Name = "BrettVPC"
  }
}

# Internet Gateway
resource "aws_internet_gateway" "brett_igw" {
  vpc_id = aws_vpc.brett_vpc.id
  tags = {
    Name = "BrettInternetGateway"
  }
}

# Subnets : public
resource "aws_subnet" "public" {
  count = length(var.subnets_cidr)
  vpc_id = aws_vpc.brett_vpc.id
  cidr_block = element(var.subnets_cidr,count.index)
  availability_zone = element(var.azs,count.index)
  map_public_ip_on_launch = true
  tags = {
    Name = "BrettSubnet${count.index+1}"
  }
}

# Route table: attach Internet Gateway 
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.brett_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.brett_igw.id
  }
  tags = {
    Name = "BrettPublicRouteTable"
  }
}

# Route table association with public subnets
resource "aws_route_table_association" "rta" {
  count = length(var.subnets_cidr)
  subnet_id      = element(aws_subnet.public.*.id,count.index)
  route_table_id = aws_route_table.public_rt.id
}

######
resource "aws_autoscaling_group" "web" {
  name = "${aws_launch_configuration.web.name}-asg"

  min_size             = 2
  desired_capacity     = 5
  max_size             = 7
  
  health_check_type    = "ELB"
  load_balancers = [
    aws_elb.brett-elb.id
  ]

  launch_configuration = aws_launch_configuration.web.name

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  #vpc_zone_identifier  = [
  #  aws_subnet.public_us_east_1a.id,
  #  aws_subnet.public_us_east_1b.id
  #]

  # Required to redeploy without an outage.
  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }

}

